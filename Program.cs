﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using MMB.ConApp.DynamoDB.Model;

namespace MMB.ConApp.DynamoDB
{
    class Program
    {
        static void Main(string[] args)
        {
            QueryTable("Customer", "2A877AF1-8059-44A1-B725-9D3239B6DFA4");
        }

        public static void QueryTable(string tableName, string id)
        {
            var customers = new List<Customer>();
            string key = tableName + "Id";
            string index = key + "-index";
            
            AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAX23NFZHRITLGEB37", "O5dY71yIPCfdwog0euY8Y/XsWPFrBjWDS9KJxDgd");
            AmazonDynamoDBConfig amazonDynamoDBConfig = new AmazonDynamoDBConfig() { RegionEndpoint = Amazon.RegionEndpoint.USEast2 };
            var dbClient = new AmazonDynamoDBClient(awsCredentials, amazonDynamoDBConfig);

            var dbContext = new DynamoDBContext(dbClient);
            var dbConfig = new DynamoDBOperationConfig() { OverrideTableName = tableName, IndexName = index };

            customers = Task.Run(() => dbContext.QueryAsync<Customer>(id, dbConfig).GetRemainingAsync()).GetAwaiter().GetResult();
            
            foreach(var customer in customers)
            {
                Console.WriteLine(customer.firstName + " " + customer.lastName + "(" + customer.customerId + ")");
            }
            
        }
    }
}
