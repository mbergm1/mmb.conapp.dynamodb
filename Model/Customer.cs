﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazon.DynamoDBv2.DataModel;

namespace MMB.ConApp.DynamoDB.Model
{
    [DynamoDBTable("Customer")]
    public class Customer
    {
        [DynamoDBHashKey("CustomerId")]
        public string customerId { get; set; }

        [DynamoDBProperty("LastName")]
        public string lastName { get; set; }

        [DynamoDBProperty("FirstName")]
        public string firstName { get; set; }
    }
}
